# Demo

[Click here to see demo](https://innoloft-custom-dashboard.netlify.app/)

![Demo Image](https://i.ibb.co/CvjMS8g/innoloft-custom-dashboard.jpg "Dashboard")


# Task

Create a [React](https://reactjs.org) application that provides the features described in this document.
The basic requests of the task must be fulfilled. Further creative approaches are of course always desired and a great bonus.

# Description


In the first tab (basic data):

* [x] - Change e-mail address
* [x] - Change Password
* [x] - The password must have certain properties:
* [x]   - "Password" and "Password repeat" fields need to be identical (including an indicator for this equality)
* [x]   - The password field should accept Uppercase letters, lowercase letters, numbers and special characters
* [x]   - A multi-color password strength indicator should be implemented
* [x] - Button to update the user data


In the second tab (Address):

* [x] - Change first name
* [x] - Change Last Name
* [x] - Change address (street, house number, postal code)
* [x] - Change country (Germany, Austria, Switzerland are available)
* [x] - Button to update the user data


* [x] The form also needs a button to submit the information through a fake AJAX call.
* 
* [x] It is important that the user receives feedback if his input is incorrect, correct, his data has not been saved and his data has been successfully saved.
* 
* [x] The layout also contains a header and a sidebar, which are not functioning in this test.


# Layout


Please note that this dashboard should be responsive, and **be usable on mobile and tablets** as well.

However, bonus points are awarded if the styles resemble those of [energieloft.de](https://energieloft.de) and could fit as seamlessly as possible within that website.


# Code structure

The application should at the very least use the following:

* [x] - React.js framework
* [x] - A CSS pre-compiler (SASS, LESS, SCSS) or other CSS approaches (CSS modules, Styled components)


* [x]  You can use external modules like [Redux](https://redux.js.org), [informed](https://joepuzzo.github.io/informed/), and whatever you think is necessary.

In React, the application is to be assembled into suitable, reusable React components.


* [x] Possible components would be: 

- Header
- Footer
- User
- UserInputTabs
- Aside

It is however up to you to provide the structure you think works best in a production-level app.


* [x]  Bonus points would be awarded for setting up and using linters.

# Deadline

Spent 12 hours on this project: 

Approximately 4hs on layout and dependencies, 4hs on components and styles and 4hs on setting redux state, implementing logic and bug fixing.



