export const fakePostRequest = (obj) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(obj);
      }, 1500);
    });
  };