import { userTypes } from "./user.types";

export const setAddressInformation = ({
  street,
  houseNumber,
  postalCode,
  firstName,
  lastName,
  country,
}) => ({
  type: userTypes.SET_ADDRESS_INFORMATION,
  payload: { street, houseNumber, postalCode, firstName, lastName, country },
});

export const setEmail = ({ email }) => ({
  type: userTypes.SET_EMAIL,
  payload: email,
});
