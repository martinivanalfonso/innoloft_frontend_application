import { createSelector } from 'reselect'

export const selectUserInformation = state => state.user

export const selectUserEmail = createSelector(
    [selectUserInformation],
    (user) => user.email
)
export const selectUserName = createSelector(
    [selectUserInformation],
    (user) => user.firstName
)