import { userTypes } from './user.types'

const INITIAL_STATE = {
    email: "currentemail@example.com",
    firstName: "Jake",
    lastName: "Riepma",
    street: "Goldway Rd",
    houseNumber: "34",
    postalCode: "5150",
    country: "Switzerland",
}

const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case userTypes.SET_ADDRESS_INFORMATION:
            return {
                ...state,
                firstName: action.payload.firstName,
                lastName: action.payload.lastName,
                street: action.payload.street,
                houseNumber: action.payload.houseNumber,
                postalCode: action.payload.postalCode,
                country: action.payload.country,
            }
        case userTypes.SET_EMAIL:
            return {
                ...state,
                email: action.payload.email,
            }
    
        default:
            return state;
    }
}

export default userReducer