import React from "react";
import Header from "./components/header/header.component";
import Sidebar from "./components/sidebar/sidebar.component";
import PageUser from "./pages/page-user.component";
import PageWrap from "./components/page-wrap/page-wrap.component";

import { GlobalStyle } from "./global.style";

const App = () => (
  <div>
    <GlobalStyle />
    <Header />
    <PageWrap>
      <Sidebar />
      <PageUser />
    </PageWrap>
  </div>
);

export default App;
