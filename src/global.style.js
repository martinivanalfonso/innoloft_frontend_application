import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
body {
    padding: 0;
    margin: 0;
}
a {
    text-decoration: none;

}
*{
    box-sizing: border-box;
    font-family: 'Roboto', sans-serif;
}

button {
    cursor: pointer;
    border: none;
}
`;