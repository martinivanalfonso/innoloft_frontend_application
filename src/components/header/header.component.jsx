import React, { useState } from "react";
import styled from "styled-components";
import { FaBell, FaUserCog, FaGlobe, FaGripLines } from "react-icons/fa";
import { connect } from 'react-redux'
import { createStructuredSelector} from 'reselect'

import { selectUserName } from '../../redux/user/user.selectors'

import DrowpdownMenu from '../../components/dropdown-menu/dropdown-menu.component'
const HeaderContainer = styled.div`
  width: 100%;
  height: 80px;
  display: flex;
  justify-content: space-between;
  box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.5);
  background-color: white;
  position: fixed;
`;

const LogoContainer = styled.div`
  width: 180px;
  height: 100%;
  background-image: url("https://img.innoloft.de/logo.svg");
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
  margin-left: 20px;
`;
const IconsContainer = styled.div`
  width: 200px;
  height: 100%;
  background-color: #e4b302;
  display: flex;
  align-items: center;
  justify-content: center;
  border-left-radius: 100%;
  margin-left: 10px;
`;
const Icon = styled.span`
  margin: 10px;
  cursor: pointer;
`;
const IconDropDown = styled.span`
  margin: 10px;
  cursor: pointer;
  @media screen and (min-width: 800px) {
    display:none;
}
`;

const HeaderHeight = styled.div`
  width: 100%;
  height: 80px;
`;
const HiText = styled.div`
  @media screen and (max-width: 800px) {
    display:none;
}
`;

const Header = ({ userName }) => {
  const [dropdownActive, setDropdownActive] = useState(false)
  return(
  <div>
    <HeaderContainer>
      <LogoContainer />
      <IconsContainer>
        <HiText>Hi {userName}</HiText>
        <Icon as={FaBell} />
        <Icon as={FaUserCog} />
        <Icon as={FaGlobe} />
        <IconDropDown as={FaGripLines} onClick={() => setDropdownActive(!dropdownActive)}/>
      </IconsContainer>
    </HeaderContainer>
    <HeaderHeight />
    { dropdownActive && <DrowpdownMenu />}
  </div>
)};


const mapStateToProps = createStructuredSelector({
  userName: selectUserName
})

export default connect(mapStateToProps)(Header);
