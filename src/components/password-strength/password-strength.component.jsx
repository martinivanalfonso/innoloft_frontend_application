import React from "react";
import styled from "styled-components";
import PropTypes from 'prop-types'

const calculateStrength = password => {
    let strength = 0    
    if (!password.length) return strength
    if (password.length < 6) return 1
    
      if (password.length >= 6) strength += 1
    
      if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
    
      if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1
    
      if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
    
    return strength
}

const PasswordStrengthContainer = styled.div`
max-width: 150px;
height: auto;
background-color: ${props => props.strength ? 'white' : 'gray'};
display:flex;
border: 1px solid gray;
border-radius: 2px;
margin: 10px 0px;
`;

const PasswordStrengthFill = styled.div`
width: ${props => props.width[props.strength]};
height: 25px;
background-color: ${props => props.colours[props.strength]};
`;

const PasswordStrength = ({ password }) => {
    const strength = calculateStrength(password)
    const colours = ['red','orange','yellow','green','green']
    const width = ['0%','30%','50%','80%','100%']
  return (
    <PasswordStrengthContainer strength={strength}>
      <PasswordStrengthFill strength={strength} colours={colours} width={width} />
    </PasswordStrengthContainer>
  );
};

PasswordStrength.propTypes = {
  password: PropTypes.string,
}

export default PasswordStrength;