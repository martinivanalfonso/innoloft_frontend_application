import React, { useState } from "react";
import styled from "styled-components";

import AddressChangeForm from "../form-custom-tabs/address-change-form.component";
import EmailAndPasswordChangeForm from "../form-custom-tabs/email-and-password-change-form.component";

const FormContainer = styled.div`
  max-width: 800px;
  width: 100%;
  height: auto;
  border: 1px solid rgba(0, 0, 0, 0.3);
  box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.5),
  -5px 5px 5px rgba(0, 0, 0, 0.5);

`;
const FormsContainer = styled.div`
  width: 100%;
  @media screen and (min-width: 1100px) {
    padding: 0px 100px;
}
`;
const TabsContainer = styled.div`
  width: 100%;
  height: 50px;
  display: flex;
  flex-direction: columns;
`;

const TabLeft = styled.button`
width:100%;
border: none;
background-color: ${props => props.currentTab === 'left' ? 'white' : 'gray'};

`
const TabRight = styled.button`
width:100%;
border: none;
background-color: ${props => props.currentTab === 'right' ? 'white' : 'gray'};
`

const Form = () => {
  const [tab, setTab] = useState("left");
  return (
    <FormContainer>
      <TabsContainer>
        <TabLeft currentTab={tab} onClick={() => setTab("left")}>Main Information</TabLeft>
        <TabRight currentTab={tab} onClick={() => setTab("right")}>Additional Information</TabRight>
      </TabsContainer>
      <FormsContainer>
        {tab === "left" && <EmailAndPasswordChangeForm />}
        {tab === "right" && <AddressChangeForm />}
      </FormsContainer>
    </FormContainer>
  );
};

export default Form;
