import React from "react";
import styled from "styled-components";
import { FaHome, FaUser, FaBuilding, FaNewspaper, FaChartLine, FaTools  } from "react-icons/fa";


const SidebarContainer = styled.div`
  width: 200px;
  background-color: #272e71;
  height: 1000px;
  color: white;

  @media screen and (max-width: 800px) {
    display:none;
}
`;
const SidebarTitle = styled.div`
  margin: 10px;
  width: 100%;
  padding: 10px;
  font-size: 0.8rem;
`;
const SidebarOption = styled.button`
  width: 100%;
  background-color: ${(props) => (props.activeOption ? "white" : "transparent")};
  color: ${(props) => (props.activeOption ? "black" : "white")};
  padding-left: 50px;
  text-transform: uppercase;
  display: flex;
  align-items: center;

  & :first-child {
    margin: 10px;
  }
`;

const Sidebar = () => (
    <SidebarContainer>
      <SidebarTitle>Options</SidebarTitle>
      <SidebarOption>
        <FaHome />
        <p>Home</p>
      </SidebarOption>
      <SidebarOption activeOption>
        <FaUser />
        <p>My Account</p>
      </SidebarOption>
      <SidebarOption>
        <FaBuilding />
        <p>My Company</p>
      </SidebarOption>
      <SidebarOption>
        <FaTools />
        <p>My Settings</p>
      </SidebarOption>
      <SidebarOption>
        <FaNewspaper />
        <p>News</p>
      </SidebarOption>
      <SidebarOption>
        <FaChartLine />
        <p>Analytics</p>
      </SidebarOption>
    </SidebarContainer>
  );

export default Sidebar;
