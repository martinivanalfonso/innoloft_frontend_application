import React from "react";
import styled from 'styled-components'

const PageWrapContainer = styled.div`
width: 100%; 
display: grid;
grid-template-columns: 190px 1fr; 
@media screen and (max-width: 800px) {
    grid-template-columns: 1fr; 
}
`
 
const PageWrap = ({children}) => (
    <PageWrapContainer>
        {children}
    </PageWrapContainer>
)

export default PageWrap;
