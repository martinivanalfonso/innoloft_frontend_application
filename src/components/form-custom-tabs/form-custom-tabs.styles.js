import styled from 'styled-components'

export const CustomFormContainer = styled.div`
width: 100%;
padding: 20px 40px;
display: flex;
flex-direction: column;
`;

export const Input = styled.input`
padding: 10px;
`;
export const Button = styled.button`
margin: 20px;
padding: 15px;
text-transform: uppercase;
background-color: #272e71;
color: white;
`;

export const InputLabel = styled.label`
pointer-events: none;
margin-top: 20px;
font-size: 0.8rem;
`;

export const ErrorMessage = styled.p`
color: red;
font-size: 0.8rem;
`;
