import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import PropTypes from 'prop-types'

import { setEmail } from "../../redux/user/user.actions";
import { selectUserEmail } from "../../redux/user/user.selectors";
import PasswordStrength from "../password-strength/password-strength.component";
import { fakePostRequest } from "../../utils.js";

import {
  CustomFormContainer,
  Input,
  Button,
  InputLabel,
  ErrorMessage,
} from "./form-custom-tabs.styles";

const EmailAndPasswordChangeForm = ({ currentEmail, setNewEmail }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordRepeat, setPasswordRepeat] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [submitMessage, setSubmitMessage] = useState("");

  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [passwordRepeatError, setPasswordRepeatError] = useState("");

  useEffect(() => {
    password.length && password.length < 6
      ? setPasswordError("Password is too short and weak")
      : setPasswordError("");

    password !== passwordRepeat
      ? setPasswordRepeatError("Passwords don't match")
      : setPasswordRepeatError("");
  }, [password, passwordRepeat]);

  useEffect(() => {
    const re = /\S+@\S+\.\S+/;
    email.length && !re.test(email)
      ? setEmailError("Email is not valid")
      : setEmailError("");
  }, [email]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      !emailError &&
      !passwordError &&
      !passwordRepeatError &&
      email &&
      password &&
      passwordRepeat
    ) {
      try {
        setIsSubmitting(true);
        setSubmitMessage("");
        await fakePostRequest({ email });
        setNewEmail({ email });
        setSubmitMessage("Changes succesfully saved");
        setIsSubmitting(false);
      } catch (error) {
        setSubmitMessage("Request failed, try again later.");
        console.log(error);
        setIsSubmitting(false);
      }
    } else {
      setSubmitMessage("All fields are required");
    }
  };

  return (
    <CustomFormContainer>
      <InputLabel>E-mail</InputLabel>
      <Input
        name="email"
        type="email"
        value={email}
        placeholder={currentEmail}
        onChange={(e) => setEmail(e.target.value)}
      />
      {emailError && <ErrorMessage>{emailError}</ErrorMessage>}

      <InputLabel>Password</InputLabel>

      <Input
        name="password"
        type="password"
        value={password}
        placeholder="Password"
        onChange={(e) => setPassword(e.target.value)}
      />
      {passwordError && <ErrorMessage>{passwordError}</ErrorMessage>}
      <InputLabel>Repeat Password</InputLabel>

      <Input
        name="passwordRepeat"
        type="password"
        value={passwordRepeat}
        placeholder="Repeat Password"
        onChange={(e) => setPasswordRepeat(e.target.value)}
      />
      {passwordRepeatError && (
        <ErrorMessage>{passwordRepeatError}</ErrorMessage>
      )}

      <InputLabel>Password Strength</InputLabel>

      <PasswordStrength password={password} />
      <Button type="submit" onClick={(e) => handleSubmit(e)}>
        {isSubmitting ? "Submitting changes..." : "Save Changes"}
      </Button>
      {submitMessage && <ErrorMessage>{submitMessage}</ErrorMessage>}
    </CustomFormContainer>
  );
};

EmailAndPasswordChangeForm.propTypes = {
  currentEmail: PropTypes.string,
  setNewEmail: PropTypes.func,
}

const mapDispatchToProps = (dispatch) => ({
  setNewEmail: (email) => dispatch(setEmail(email)),
});
const mapStateToProps = createStructuredSelector({
  currentEmail: selectUserEmail,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmailAndPasswordChangeForm);
