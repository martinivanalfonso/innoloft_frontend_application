import React, { useState } from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import PropTypes from 'prop-types'

import { setAddressInformation } from "../../redux/user/user.actions";
import { selectUserInformation } from "../../redux/user/user.selectors";
import { fakePostRequest } from "../../utils.js";

import {
  CustomFormContainer,
  Input,
  Button,
  InputLabel,
  ErrorMessage,
} from "./form-custom-tabs.styles";

const AddressChangeForm = ({ current, setAddressInformation }) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [street, setStreet] = useState("");
  const [houseNumber, setHouseNumber] = useState("");
  const [postalCode, setPostalCode] = useState("");
  const [country, setCountry] = useState(current.country);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [submitMessage, setSubmitMessage] = useState("");

  const handleSubmit = async () => {
    if (firstName && lastName && street && houseNumber && postalCode && country) {
      try {
        setIsSubmitting(true);
        await fakePostRequest({ street, houseNumber, postalCode, firstName, lastName, country });
        setAddressInformation({ street, houseNumber, postalCode, firstName, lastName, country });
        setSubmitMessage("Changes succesfully saved");
        setIsSubmitting(false);
      } catch (error) {
        setSubmitMessage("Request failed, try again later.");
        setIsSubmitting(false);
      }
    } else {
      setSubmitMessage("All fields are required");
    }
  };
  return (
    <CustomFormContainer>
      <InputLabel>First Name</InputLabel>
      <Input
        name="firstName"
        type="text"
        value={firstName}
        placeholder={current.firstName}
        onChange={(e) => setFirstName(e.target.value)}
      />
      <InputLabel>Last Name</InputLabel>
      <Input
        name="lastName"
        type="text"
        value={lastName}
        placeholder={current.lastName}
        onChange={(e) => setLastName(e.target.value)}
      />
      <InputLabel>Address:</InputLabel>
      <InputLabel>Street</InputLabel>
      <Input
        name="street"
        type="text"
        value={street}
        placeholder={current.street}
        onChange={(e) => setStreet(e.target.value)}
      />
      <InputLabel>House Number</InputLabel>
      <Input
        name="houseNumber"
        type="text"
        value={houseNumber}
        placeholder={current.houseNumber}
        onChange={(e) => setHouseNumber(e.target.value)}
      />
      <InputLabel>Postal Code</InputLabel>
      <Input
        name="postalCode"
        type="number"
        value={postalCode}
        placeholder={current.postalCode}
        onChange={(e) => setPostalCode(e.target.value)}
      />
      
      <InputLabel>Country</InputLabel>
      <Input
        name="country"
        as="select"
        value={country}
        placeholder={current.country}
        onChange={(e) => setCountry(e.target.value)}
      >
        <option>Germany</option>
        <option>Austria</option>
        <option>Switzerland</option>
      </Input>
      <Button type="submit" onClick={() => handleSubmit()}>
        {isSubmitting ? "Submitting changes..." : "Save Additional Changes"}
      </Button>
      {submitMessage && <ErrorMessage>{submitMessage}</ErrorMessage>}
    </CustomFormContainer>
  );
};

AddressChangeForm.propTypes = {
  current: PropTypes.object,
  setAddressInformation: PropTypes.func,
}

const mapDispatchToProps = (dispatch) => ({
  setAddressInformation: (information) =>
    dispatch(setAddressInformation(information)),
});
const mapStateToProps = createStructuredSelector({
  current: selectUserInformation,
});

export default connect(mapStateToProps, mapDispatchToProps)(AddressChangeForm);
