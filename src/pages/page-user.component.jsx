import React from "react";
import styled from "styled-components";
import { FaUser } from "react-icons/fa";

import Form from "../components/form/form.component";

const PageUserContainer = styled.div`
  width: 100%;
  height: 900px;
  padding: 20px 80px;
  @media screen and (max-width: 800px) {
    padding: 10px;
}
`;

const UserTitleContainer = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
  & :first-child {
    font-size: 1.3rem;
    margin: 10px;
  }
`;

const PageUser = () => (
  <PageUserContainer>
    <UserTitleContainer>
    <FaUser />
    <h2>Profile Details</h2>
    </UserTitleContainer>
    <Form />
  </PageUserContainer>
);

export default PageUser;
